<?php

header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: post-check=0, pre-check=0", false);
session_cache_limiter("must-revalidate");
session_start();
include 'config.php';

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="src/images/favicon.png"/>
    <title>
        <?php

        if (isset($_GET['schedule'])) : echo 'Розклад | Online-сервіс супроводу студентів-заочників';
        elseif (isset($_GET['groups'])) : echo 'Списки груп | Online-сервіс супроводу студентів-заочників';
        elseif (isset($_GET['rating'])) : echo 'Рейтинг | Online-сервіс супроводу студентів-заочників';
        elseif (isset($_GET['scholarship'])) : echo 'Стипендія | Online-сервіс супроводу студентів-заочників';
        elseif (isset($_GET['log'])) : echo 'Журнал | Online-сервіс супроводу студентів-заочників';
        elseif (isset($_GET['zalik'])) : echo 'е-Заліковка | Online-сервіс супроводу студентів-заочників';
        elseif (isset($_GET['search'])) : echo 'Пошук пошти | Online-сервіс супроводу студентів-заочників';
        else : echo 'Online-сервіс супроводу студентів-заочників.';
        endif;

        ?>
    </title>
    <link href="src/libs/bootstrap.min.css" rel="stylesheet">
    <link href="src/libs/bootstrap-table.min.css" rel="stylesheet">
    <link href="src/css/style.css" rel="stylesheet">
</head>
<body>
<header>
    <nav class="navbar navbar-expand-lg bg-light">
        <div class="container">
            <a class="navbar-brand" href="./">
                <img width="50" height="50" src="src/images/favicon.png" alt="Logo"/>
            </a>
            <a href="pppi-admin/" target="_blank">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="#000" class="bi bi-person"
                     viewBox="0 0 16 16">
                    <path d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6Zm2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0Zm4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4Zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10Z"/>
                </svg>
            </a>
        </div>
    </nav>
</header>

<main>

    <?php

    if (!$_GET) include 'template-pages/home.php';

    if (isset($_GET['schedule'])) include 'template-pages/schedule.php';
    if (isset($_GET['groups'])) include 'template-pages/groups.php';
    if (isset($_GET['rating'])) include 'template-pages/rating.php';
    if (isset($_GET['scholarship'])) include 'template-pages/scholarship.php';
    if (isset($_GET['zalik'])) include 'template-pages/zalik.php';
    if (isset($_GET['search'])) include 'template-pages/search.php';
    if (isset($_GET['log'])) include 'template-pages/log.php';

    ?>

</main>

<footer>

</footer>

<script src="src/libs/jquery.min.js"></script>
<script src="src/libs/bootstrap.bundle.min.js"></script>
<script src="src/libs/bootstrap-table.min.js"></script>
<script src="src/libs/bootstrap-table-filter-control.min.js"></script>

<script>
    $(function () {
        const table = document.querySelectorAll('#table');

        for (let i = 0; i < table.length; i++) {
            $(table[i]).bootstrapTable()
        }
    })
</script>
</body>
</html>