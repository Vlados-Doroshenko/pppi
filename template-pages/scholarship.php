<?php
$queryScholarship = mysqli_query($db, "SELECT scholarship_setting FROM settings");

while ($resScholarship = mysqli_fetch_assoc($queryScholarship)) :
    if ($resScholarship['scholarship_setting']) : ?>

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <iframe src="<?= $resScholarship['scholarship_setting']; ?>" width="100%" height="800"></iframe>
                    </div>
                </div>
            </div>
        </section>

    <?php endif;

endwhile; ?>