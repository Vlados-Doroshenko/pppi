<section class="home">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-4">
                <a href="?schedule" class="link-card">
                    <div class="card">
                        <img src="src/images/schedule.jpg" class="card-img-top" alt="Розклад">
                        <div class="card-body">
                            <h5 class="card-title">Розклад</h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-4">
                <a href="?groups" class="link-card">
                    <div class="card">
                        <img src="src/images/spisok.jpg" class="card-img-top" alt="Списки груп">
                        <div class="card-body">
                            <h5 class="card-title">Списки груп</h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-4">
                <a href="?rating" class="link-card">
                    <div class="card">
                        <img src="src/images/rating.jpg" class="card-img-top" alt="Рейтинг">
                        <div class="card-body">
                            <h5 class="card-title">Рейтинг</h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-4">
                <a href="?scholarship" class="link-card">
                    <div class="card">
                        <img src="src/images/stipuha.jpg" class="card-img-top" alt="Стипендія">
                        <div class="card-body">
                            <h5 class="card-title">Стипендія</h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-4">
                <a href="?zalik" class="link-card">
                    <div class="card">
                        <img src="src/images/zalikova-knizhka.jpg" class="card-img-top" alt="е-Заліковка">
                        <div class="card-body">
                            <h5 class="card-title">е-Заліковка</h5>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-12 col-sm-4">
                <a href="?search" class="link-card">
                    <div class="card">
                        <img src="src/images/search.jpg" class="card-img-top" alt="Пошук пошти">
                        <div class="card-body">
                            <h5 class="card-title">Пошук пошти</h5>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section>