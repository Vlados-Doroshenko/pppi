<section class="pt-5">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <ul class="nav nav-pills mb-3 justify-content-center" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-home"
                                type="button" role="tab" aria-controls="pills-home" aria-selected="true">All
                        </button>
                    </li>
                    <?php $queryGroup = mysqli_query($db, "SELECT * FROM groups");

                    while ($resGroup = mysqli_fetch_assoc($queryGroup)) : ?>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="<?= $resGroup['name_group']; ?>-tab" data-bs-toggle="pill"
                                    data-bs-target="#<?= $resGroup['name_group']; ?>-profile" type="button" role="tab"
                                    aria-controls="<?= $resGroup['name_group']; ?>-profile"
                                    aria-selected="false"><?= $resGroup['name_group']; ?></button>
                        </li>
                    <?php endwhile; ?>
                </ul>
            </div>
            <div class="col-12">
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                         aria-labelledby="pills-home-tab"
                         tabindex="0">
                        <table
                                id="table"
                                data-filter-control="true"
                                data-show-search-clear-button="false">
                            <thead>
                            <tr>
                                <th data-field="name" data-filter-control="input">ПІП</th>
                                <th data-field="email" data-filter-control="input">Email</th>
                                <th data-field="phone" data-filter-control="input">Телефон</th>
                                <th data-field="group" data-filter-control="input">Група</th>
                                <th data-field="specialty" data-filter-control="select">Спеціальність</th>
                                <th data-field="faculty" data-filter-control="select">Факультет</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $queryStudents = mysqli_query($db, "SELECT * FROM students, faculties, groups, specialty WHERE idGroup_student = id_group AND idFaculty_student = id_faculty AND idSpecialty_student = id_specialty ");

                            while ($resStundent = mysqli_fetch_assoc($queryStudents)) : ?>
                                <tr>
                                    <td>
                                        <?= $resStundent['name_student']; ?>
                                    </td>
                                    <td>
                                        <a href="mailto: <?= $resStundent['email_student']; ?>">
                                            <?= $resStundent['email_student']; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <a href="tel: <?= $resStundent['phone_student']; ?>">
                                            <?= $resStundent['phone_student']; ?>
                                        </a>
                                    </td>
                                    <td>
                                        <?php if ($resStundent['name_group'] === 'КС-19' || $resStundent['name_group'] === 'сКС-20') : ?>
                                            <a href="?log">
                                                <?= $resStundent['name_group']; ?>
                                            </a>
                                        <?php else : ?>
                                            <?= $resStundent['name_group']; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td>
                                        <?= $resStundent['name_specialty']; ?>
                                    </td>
                                    <td>
                                        <?= $resStundent['name_faculty']; ?>
                                    </td>
                                </tr>
                            <?php endwhile; ?>
                            </tbody>
                        </table>
                    </div>

                    <?php $queryGroups = mysqli_query($db, "SELECT * FROM groups");

                    while ($resGroups = mysqli_fetch_assoc($queryGroups)) : ?>
                        <div class="tab-pane fade" id="<?= $resGroups['name_group']; ?>-profile" role="tabpanel"
                             aria-labelledby="<?= $resGroups['name_group']; ?>-tab" tabindex="0">
                            <table
                                    id="table"
                                    data-filter-control="true"
                                    data-show-search-clear-button="false">
                                <thead>
                                <tr>
                                    <th data-field="name" data-filter-control="input">ПІП</th>
                                    <th data-field="email" data-filter-control="input">Email</th>
                                    <th data-field="phone" data-filter-control="input">Телефон</th>
                                    <th data-field="group" data-filter-control="input">Група</th>
                                    <th data-field="specialty" data-filter-control="select">Спеціальність</th>
                                    <th data-field="faculty" data-filter-control="select">Факультет</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $queryStudent = mysqli_query($db, "SELECT * FROM students, faculties, groups, specialty WHERE idGroup_student = {$resGroups['id_group']} AND idGroup_student = id_group AND idFaculty_student = id_faculty AND idSpecialty_student = id_specialty ");

                                while ($student = mysqli_fetch_assoc($queryStudent)) : ?>
                                    <tr>
                                        <td>
                                            <?= $student['name_student']; ?>
                                        </td>
                                        <td>
                                            <a href="mailto: <?= $student['email_student']; ?>">
                                                <?= $student['email_student']; ?>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="tel: <?= $student['phone_student']; ?>">
                                                <?= $student['phone_student']; ?>
                                            </a>
                                        </td>
                                        <td>
                                            <?php if ($student['name_group'] === 'КС-19' || $student['name_group'] === 'сКС-20') : ?>
                                            <a href="?log">
                                                <?= $student['name_group']; ?>
                                            </a>
                                            <?php else : ?>
                                                <?= $student['name_group']; ?>
                                            <?php endif; ?>
                                        </td>
                                        <td>
                                            <?= $student['name_specialty']; ?>
                                        </td>
                                        <td>
                                            <?= $student['name_faculty']; ?>
                                        </td>
                                    </tr>
                                <?php endwhile; ?>
                                </tbody>
                            </table>
                        </div>
                    <?php endwhile; ?>
                </div>
            </div>
        </div>
    </div>
</section>