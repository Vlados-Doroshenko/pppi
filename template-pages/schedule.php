<?php
$querySchedule = mysqli_query($db, "SELECT schedule_setting FROM settings");

while ($resSchedule = mysqli_fetch_assoc($querySchedule)) :
    if ($resSchedule['schedule_setting']) : ?>

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <iframe src="<?= $resSchedule['schedule_setting']; ?>" width="100%" height="800"></iframe>
                    </div>
                </div>
            </div>
        </section>

    <?php endif;

endwhile; ?>