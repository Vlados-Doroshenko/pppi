<?php
$queryRating = mysqli_query($db, "SELECT rating_setting FROM settings");

while ($resRating = mysqli_fetch_assoc($queryRating)) :
    if ($resRating['rating_setting']) : ?>

        <section>
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <iframe src="<?= $resRating['rating_setting']; ?>" width="100%" height="800"></iframe>
                    </div>
                </div>
            </div>
        </section>

    <?php endif;

endwhile; ?>