<?php
if (isset($_GET['delPicMat'])) { unlink($_GET['delPicMat']); echo "<script> document.location.href='?response'; </script>";}

if (isset($_POST['addPicMat'])) {     
    
    include 'picter_response.php';
    include 'resemp_response.php';
    @mkdir("../images/response/".$dir, 0777);
    $dirNew = "../images/response/".$dir."/";
    //$fots = array_slice(scandir($dirNew), 2);
    //$fots = glob($dirNew."*.jpg");
    $fots = glob("../images/response/*.jpg");
    natsort($fots);
    $fil=array_pop($fots);
    //print "<script> alert('".$fil."'); </script>";
    $fil1 = explode('/',$fil);
    $fil=array_pop($fil1);
    $fill = explode('.',$fil);
    $count = 0;
    $count = $fill[0];

    foreach($_FILES["filename"]["name"] as $k=>$v) {
        $count++;
        $newname = $count.'.jpg';

        $pic = new Picter_Response();
        $newPic = new Resemp_Response($pic);
        $pic->ustanovka($k);
        $newPic->createNewImage($pic, $dir, $newname);
        unset($pic);
        unset($newPic);
    }
           
    echo "<script> document.location.href=document.location.href; </script>";   
}
?>

<div class="container">
    <div class="title-table">
        <h3 class="text-center" title='Відгуки'><i class="fas fa-comments fa-lg"></i> Відгуки</h3>
    </div>
    <div class="row">
        <div class="shapka" id="toolbar1">
           <!-- <button id="show" title="Залишити відмічені" class="btn btn-secondary">
                <i class="far fa-check-square fa-lg"></i>
            </button> -->
           <!-- <a title="Додати матеріал" class="btn btn-secondary addRiel" href="?portfolio&addMat">
               <i class="fas fa-plus fa-lg"></i> Додати матеріал
            </a>   -->         
        </div>
        <div id="table1" class="table-responsive">
            <form action="" method="post" enctype="multipart/form-data">
                    <div class="addMatImg text-center">
                        <div class="title_mod">
                            <?php
                                $masss2 = glob("../images/response/*.jpg");
                                natcasesort($masss2);
                                $count = 0;
                                foreach ($masss2 as $fn) {
                                    $count++;
                                }                                
                            ?>
                            <h5 title="Зображення"><i class="far fa-images fa-lg"></i> Зображення (Всього: <?php echo $count; ?>)</h5>
                        </div>
                        <?php
                            $masss1 = glob("../images/response/*.jpg");
                            natcasesort($masss1);
                            $count = 0;
                            foreach ($masss1 as $fn) {
                        ?>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 d-inline-block podfoto text-center">
                            <a data-toggle="tooltip" title="Видалити це фото" href="<?php echo "?response&delPicMat=".$fn; ?>" class="trash"><i class="far fa-trash-alt"></i></a>
                            <img class="fotoListMediaEd" src='<?php echo $fn."?".date('is'); ?>'>
                        </div>
                        <?php } ?>
                    </div>                    
                    <div class="addMatImg">
                        <div class="title_mod">
                            <h5 title="Додати зображення"><i class="far fa-image fa-lg"></i></i> Додати зображення</h5>
                        </div>
                        <div class="input-group mb-3" title="Виберіть файл">                            
                            <div class="custom-file">
                                 <input type="file" name="filename[]" class="custom-file-input" id="customFileLang-<?php echo$_GET['editPicMat']; ?>" onchange='document.querySelector(".custom-file-label").innerHTML = Array.from(this.files).map(f => f.name).join(" | ")' multiple="true">
                                 <label class="custom-file-label" for="customFileLang-<?php echo $_GET['editPicMat']; ?>" data-browse="Огляд">Виберіть файл</label>
                            </div>                        
                        </div>                        
                    </div>                    
                    <hr>
                    <button class="btn btn-info btn-block" title="Додати зображення" type="submit" name="addPicMat"><i class="fas fa-plus fa-lg"></i> Додати</button>
                </form>
        </div>

    </div>
</div>