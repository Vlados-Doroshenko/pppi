<?php if (isset($_GET['addFaculty'])) { ?>
    <script> $(document).ready(function () {
            $('#addFaculty').modal('show');
        });</script>
<?php } ?>

<?php
if (isset($_POST['addFaculty'])) {

    $name = (isset($_POST['name_faculty'])) ? mysqli_real_escape_string($db, $_POST['name_faculty']) : '';

    mysqli_query($db, "INSERT  INTO faculties
        SET
        name_faculty = '{$name}' ");
    echo "<script>$.confirm({title: 'Успішно!', content: 'Факультет {$name} додано', buttons: { OK: function() { document.location.href='?faculties';}} });</script>";
}
?>
<div class="modal fade" id="addFaculty" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" title="Додати факультет"><i class="fa fa-university fa-lg"></i> Додати факультет</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post">
                    <div class="input-group mb-3" title="Назва факультета">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-signature"></i></span>
                        </div>
                        <input type="text" class="form-control" name="name_faculty" placeholder="Назва факультета*"
                               autocomplete="off" required>
                    </div>
                    <hr>
                    <button class="btn btn-info btn-block" title="Додати" type="submit" name="addFaculty"><i
                            class="fas fa-folder-plus"></i> Додати
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>