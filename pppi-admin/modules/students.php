<?php
if (isset($_GET['delStudent'])) {
    $educ = mysqli_query($db, "DELETE FROM students WHERE id_student ='" . $_GET['delStudent'] . "'");
    echo "<script> document.location.href='?students'; </script>";
}
$educ = "SELECT * FROM students, specialty, groups, faculties WHERE id_group = idGroup_student AND id_faculty = idFaculty_student AND id_specialty = idSpecialty_student";
?>

<div class="container">
    <div class="title-table">
        <h3 class="text-center" title='Освіта'><i class="fas fa-user-graduate fa-lg"></i> Студенти</h3>
    </div>
    <div class="row">
        <div class="shapka" id="toolbar1">
            <a title="Додати" class="btn btn-secondary addRiel" href="?students&addStudent">
                <i class="fas fa-plus fa-lg"></i> Додати
            </a>
        </div>
        <div id="table1" class="table-responsive">
            <table class="table table-striped table-condensed table-hover"
                   data-locale="uk-UA"
                   id="table111"
                   data-toggle="table111"
                   data-show-toggle="false"
                   data-toolbar="#toolbar1"
                   data-show-fullscreen="false"
                   data-filter-control="true"
                   data-filter-show-clear="false"
                   data-show-print="true"
                   data-sort-name="id"
                   data-sort-order="asc"
                   data-show-copy-rows="false"
                   data-show-export="true"
                   data-click-to-select="false"
                   data-pagination="true"
                   data-page-list="[10, 25, 50, 100, 250, 500]"
                   data-maintain-selected="true"
                   data-maintain-meta-data="true"
                   data-show-refresh="false"
                   data-show-columns="true"
                   data-show-search-button="false"
                   data-show-search-clear-button="true"
                   data-unique-id="id"
                   data-minimum-count-columns="1"
                   data-detail-view="false"
                   data-mobile-responsive="true"
                   data-check-on-init="true"
                   data-export-options='{ "fileName":"Студенти", "worksheetName":"list1" }'>
                <thead>
                <tr>
                    <th class="text-center" data-sortable="true" title="ID" data-filter-control="input"
                        data-visible="true" data-field="id">ID
                    </th>
                    <th data-sortable="true" title="ПІП" data-filter-control="input" data-visible="true"
                        data-field="name_student">ПІП
                    </th>
                    <th data-sortable="true" title="Email" data-filter-control="input" data-visible="true"
                        data-field="email_student">Email
                    </th>
                    <th data-sortable="true" title="Телефон" data-filter-control="input" data-visible="true"
                        data-field="phone_student">Телефон
                    </th>
                    <th data-sortable="true" title="Група" data-filter-control="input" data-visible="true"
                        data-field="group">Група
                    </th>
                    <th data-sortable="true" title="Факультет" data-filter-control="input" data-visible="true"
                        data-field="faculty">Факультет
                    </th>
                    <th data-sortable="true" title="Спеціальність" data-filter-control="input" data-visible="true"
                        data-field="specialty">Спеціальність
                    </th>
                    <th class="text-center" title="Видалити" data-print-ignore="true">Видалити</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $queryEduc = mysqli_query($db, $educ);
                while ($mat = mysqli_fetch_assoc($queryEduc)) {
                    ?>
                    <tr id="tr-id-<?php echo $mat['id_student']; ?>" class="tr-c-<?php echo $mat['id_student']; ?>">
                        <td title="<?php echo $mat['id_student']; ?>"><?php echo $mat['id_student']; ?></td>
                        <td title="<?php echo $mat['name_student']; ?>">
                        <span data-type="text" data-mode="popup" data-placement="top"
                              data-pk="<?php echo $mat['id_student']; ?>" data-name="name_student" class="editName">
                            <a href="?students&editStudent=<?php echo $mat['id_student']; ?>">
                                <?php echo $mat['name_student']; ?>
                            </a>
                        </span>
                        </td>
                        <td title="<?php echo $mat['email_student']; ?>">
                        <span data-type="text" data-mode="popup" data-placement="top"
                              data-pk="<?php echo $mat['id_student']; ?>" data-name="email_student" class="editName">
                            <a href="?students&editStudent=<?php echo $mat['id_student']; ?>">
                                <?php echo $mat['email_student']; ?>
                            </a>
                        </span>
                        </td>
                        <td title="<?php echo $mat['phone_student']; ?>">
                        <span data-type="text" data-mode="popup" data-placement="top"
                              data-pk="<?php echo $mat['id_student']; ?>" data-name="phone_student" class="editName">
                            <a href="?students&editStudent=<?php echo $mat['id_student']; ?>">
                                <?php echo $mat['phone_student']; ?>
                            </a>
                        </span>
                        </td>
                        <td title="<?php echo $mat['name_group']; ?>">
                        <span data-type="text" data-mode="popup" data-placement="top"
                              data-pk="<?php echo $mat['id_student']; ?>" data-name="name_group" class="editName">
                            <a href="?students&editStudent=<?php echo $mat['id_student']; ?>">
                                <?php echo $mat['name_group']; ?>
                            </a>
                        </span>
                        </td>
                        <td title="<?php echo $mat['name_faculty']; ?>">
                        <span data-type="text" data-mode="popup" data-placement="top"
                              data-pk="<?php echo $mat['id_student']; ?>" data-name="name_faculty" class="editName">
                            <a href="?students&editStudent=<?php echo $mat['id_student']; ?>">
                                <?php echo $mat['name_faculty']; ?>
                            </a>
                        </span>
                        </td>
                        <td title="<?php echo $mat['name_specialty']; ?>">
                        <span data-type="text" data-mode="popup" data-placement="top"
                              data-pk="<?php echo $mat['id_student']; ?>" data-name="name_specialty" class="editName">
                            <a href="?students&editStudent=<?php echo $mat['id_student']; ?>">
                                <?php echo $mat['name_specialty']; ?>
                            </a>
                        </span>
                        </td>
                        <td title="Видалити">
                            <a data-toggle="tooltip" title="Видалити"
                               href="<?php echo "?students&delStudent=" . $mat['id_student']; ?>"><i
                                        class="far fa-trash-alt"></i></a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>

    </div>
</div>
<?php
if (isset($_GET['editStudent'])) {
    include 'modules/mod_edit_student.php';
}
if (isset($_GET['addStudent'])) {
    include 'modules/mod_add_student.php';
}
?>
<script>
    $(document).ready(function () {

        $(function () {
            var $table1 = $('#table111'), selections1 = [], ids = [];

            function getHeight() {
                return $(window).height() - 229;
            }

            $(window).resize(function () {
                $table1.bootstrapTable('resetView', {'height': getHeight()});
            });

            $('#show').click(function () {
                $table1.bootstrapTable('togglePagination');
                $table1.bootstrapTable('checkInvert');
                var ids = $.map($table1.bootstrapTable('getSelections'), function (row) {
                    return row.id
                })
                $table1.bootstrapTable('remove', {
                    field: 'id',
                    values: ids
                })
                $table1.bootstrapTable('togglePagination');
            });

            $table1.bootstrapTable({
                height: getHeight(),
                silent: true,
                search: true,
                paginationLoop: true,
                sidePagination: 'client',
                totalRows: 1,
                pageNumber: 1,
                paginationHAlign: 'right',
                paginationVAlign: 'both',
                icons: {print: 'fa-print', export: 'fa-file-export', columns: 'fa-list', clearSearch: 'fa-trash'}

            });
            setTimeout(function () {
                $table1.bootstrapTable('resetView', {'height': getHeight()});
            }, 1000);
        });
    });
</script>