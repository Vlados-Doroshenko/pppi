<?php
if (isset($_GET['delFaculty'])) {
    $blc = mysqli_query($db, "DELETE FROM faculties WHERE id_faculty ='" . $_GET['delFaculty'] . "'");
    echo "<script> document.location.href='?faculties'; </script>";
}
$sql = "SELECT * FROM faculties";
?>
<div class="container">
    <div class="title-table">
        <h3 class="text-center" title='Факультети'><i class="fa fa-university fa-lg"></i> Факультети</h3>
    </div>
    <div class="row">
        <div class="shapka" id="toolbar1">
            <button id="show" title="Залишити відмічені" class="btn btn-secondary">
                <i class="far fa-check-square fa-lg"></i>
            </button>
            <a title="Додати факультет" class="btn btn-secondary addRiel" href="?faculties&addFaculty">
                <i class="fa fa-university fa-lg"></i> Додати факультет
            </a>
            <a title="Очистити сортування" class="btn btn-secondary" href="?faculties">
                <i class="fas fa-times fa-lg"></i>
            </a>
        </div>
        <div id="table1" class="table-responsive">
            <table class="table table-striped table-condensed table-hover"
                   data-locale="uk-UA"
                   id="table111"
                   data-toggle="table111"
                   data-show-toggle="false"
                   data-toolbar="#toolbar1"
                   data-show-fullscreen="false"
                   data-filter-control="true"
                   data-filter-show-clear="false"
                   data-show-print="true"
                   data-show-copy-rows="false"
                   data-show-export="true"
                   data-click-to-select="false"
                   data-pagination="true"
                   data-page-list="[10, 25, 50, 100, 250, 500]"
                   data-maintain-selected="true"
                   data-maintain-meta-data="true"
                   data-show-refresh="false"
                   data-show-columns="true"
                   data-show-search-button="false"
                   data-show-search-clear-button="true"
                   data-unique-id="id_a_n"
                   data-minimum-count-columns="1"
                   data-detail-view="false"
                   data-mobile-responsive="true"
                   data-check-on-init="true"
                   data-export-types="['excel', 'doc', 'pdf']"
                   data-export-options='{ "fileName":"Факультети", "worksheetName":"list1" }'>
                <thead>
                <tr>
                    <th data-field="state" data-print-ignore="true" data-checkbox="true" tabindex="0"></th>
                    <th class="idd text-center" data-sortable="true" title="ID" data-filter-control="input"
                        data-visible="true" data-field="id">ID
                    </th>
                    <th data-sortable="true" title="Назва" data-filter-control="input" data-visible="true"
                        data-field="name_faculty">Назва
                    </th>
                    <th class="text-center" title="Видалити" data-print-ignore="true">Видалити</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $queryBlock = mysqli_query($db, $sql);
                while ($blk = mysqli_fetch_assoc($queryBlock)) {
                    ?>
                    <tr id="tr-id-<?php echo $blk['id_faculty']; ?>" class="tr-c-<?php echo $blk['id_faculty']; ?>">
                        <td id="td-id-<?php echo $blk['id_faculty']; ?>"
                            class="td-c-<?php echo $blk['id_faculty']; ?>"></td>
                        <td title="<?php echo $blk['id_faculty']; ?>"><?php echo $blk['id_faculty']; ?></td>
                        <td title="<?php echo $blk['name_faculty']; ?>">
                            <span data-type="text" data-title="Назва" data-mode="popup" data-placement="top"
                                  data-pk="<?php echo $blk['id_faculty']; ?>" data-name="name_faculty"
                                  class="editName"> <?php echo $blk['name_faculty']; ?> </span>
                        </td>
                        <td title="Видалити">
                            <a data-toggle="tooltip" title="Видалити"
                               href="<?php echo "?faculties&delFaculty=" . $blk['id_faculty']; ?>"><i
                                    class="far fa-trash-alt"></i></a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>

    </div>
</div>
<?php
if (isset($_GET['addFaculty'])) {
    include 'modules/mod_add_faculty.php';
}
?>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("click", "a[data-type='pdf']", function () {
            var pdf = new jsPDF('p', 'pt', 'a4');
            $('table thead').css('color', '#000');
            pdf.addHTML($("#table111"), function () {
                pdf.save('Факультети.pdf');
                document.location.href = document.location.href;
            });
        });
    });
</script>
<script>
    function myInit() {
        $(function () {
            $('.editName').editable({
                url: 'modules/aj_faculty.php',
                showbuttons: 'right',
                success: function (response, newValue) {
                    thiss = $(this);
                    thiss.html(newValue);
                    setTimeout(function () {
                        $('#table').bootstrapTable('updateCell', {
                            index: thiss.closest('tr').data('index'),
                            field: 'description',
                            value: thiss.closest('td').html()
                        });
                    }, 100);
                    thiss.closest('td').find('.popover').addClass('displayNone');
                    $('.tooltip').tooltip('destroy');
                    if ($(window).width() <= 400) {
                        setTimeout(function () {
                            $('#table').bootstrapTable('refresh', {silent: true});
                        }, 120);
                    }
                    setTimeout(function () {
                        myInit();
                    }, 150);
                }
            });
        });
    }

</script>
<script>
    $(document).ready(function () {

        $(function () {
            var $table1 = $('#table111'), selections1 = [], ids = [];

            function getHeight() {
                return $(window).height() - 180;
            }

            $(window).resize(function () {
                $table1.bootstrapTable('resetView', {'height': getHeight()});
            });

            $('#show').click(function () {
                $table1.bootstrapTable('togglePagination');
                $table1.bootstrapTable('checkInvert');
                var ids = $.map($table1.bootstrapTable('getSelections'), function (row) {
                    return row.id
                })
                $table1.bootstrapTable('remove', {
                    field: 'id',
                    values: ids
                })
                $table1.bootstrapTable('togglePagination');
            });

            $table1.bootstrapTable({
                height: getHeight(),
                silent: true,
                search: true,
                paginationLoop: true,
                sidePagination: 'client', // client or server
                totalRows: 1, // server side need to set
                pageNumber: 1,
                pageSize: 10,
                showPrint: true,
                paginationHAlign: 'right',
                paginationVAlign: 'both',
                icons: {print: 'fa-print', export: 'fa-file-export', columns: 'fa-list', clearSearch: 'fa-trash'}

            });
            setTimeout(function () {
                $table1.bootstrapTable('resetView', {'height': getHeight()});
            }, 1000);
            setTimeout(function () {
                myInit();
            }, 1100);
        });
    });
</script>

