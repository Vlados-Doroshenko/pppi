<?php if (isset($_GET['addSpecialty'])) { ?>
    <script> $(document).ready(function () {
            $('#addSpecialty').modal('show');
        });</script>
<?php } ?>

<?php
if (isset($_POST['addSpecialty'])) {

    $name = (isset($_POST['name_specialty'])) ? mysqli_real_escape_string($db, $_POST['name_specialty']) : '';

    mysqli_query($db, "INSERT  INTO specialty
        SET
        name_specialty = '{$name}' ");
    echo "<script>$.confirm({title: 'Успішно!', content: 'Спеціальність {$name} додана', buttons: { OK: function() { document.location.href='?specialty';}} });</script>";
}
?>
<div class="modal fade" id="addSpecialty" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" title="Додати спеціальність"><i class="fa fa-graduation-cap fa-lg"></i> Додати спеціальність</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post">
                    <div class="input-group mb-3" title="Назва спеціальності">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-signature"></i></span>
                        </div>
                        <input type="text" class="form-control" name="name_specialty" placeholder="Назва спеціальності*"
                               autocomplete="off" required>
                    </div>
                    <hr>
                    <button class="btn btn-info btn-block" title="Додати" type="submit" name="addSpecialty"><i
                            class="fas fa-folder-plus"></i> Додати
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>