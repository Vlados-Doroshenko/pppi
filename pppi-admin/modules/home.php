<!-- Start Home -->
<div class="container">
    <div class="row">
        <div class="card-home col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card">
                <img src="images/users.jpg" style="width: 100%;" alt="Користувачі">
                <div class="card-body">
                    <h5 class="card-title text-center">Користувачі</h5>
                    <a href="?all_user" class="btn btn-block btn-info">Перейти <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
        <div class="card-home col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card">
                <img src="images/diplom.jpg" style="width: 100%;" alt="Студенти">
                <div class="card-body">
                    <h5 class="card-title text-center">Студенти</h5>
                    <a href="?students" class="btn btn-block btn-info">Перейти <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
        <div class="card-home col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card">
                <img src="images/group.jpg" style="width: 100%;" alt="Групи">
                <div class="card-body">
                    <h5 class="card-title text-center">Групи</h5>
                    <a href="?groups" class="btn btn-block btn-info">Перейти <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
        <div class="card-home col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card">
                <img src="images/specialty.jpg" style="width: 100%;" alt="Спеціальності">
                <div class="card-body">
                    <h5 class="card-title text-center">Спеціальності</h5>
                    <a href="?specialty" class="btn btn-block btn-info">Перейти <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
        <div class="card-home col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card">
                <img src="images/faculty.jpg" style="width: 100%;" alt="Факультети">
                <div class="card-body">
                    <h5 class="card-title text-center">Факультети</h5>
                    <a href="?faculties" class="btn btn-block btn-info">Перейти <i class="fas fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
        <div class="card-home col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card">
                <img src="images/documentation.jpg" style="width: 100%;" alt="Документи">
                <div class="card-body">
                    <h5 class="card-title text-center">Документи</h5>
                    <a href="?documentation" class="btn btn-block btn-info">Перейти <i
                                class="fas fa-arrow-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Home -->