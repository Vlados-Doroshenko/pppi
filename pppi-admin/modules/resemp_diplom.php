<?php
class Resemp_Diplom extends Picter_Diplom{
    public $ratio_orig;
    public $image;
    public $image_p;

    /*function Resemp(){
       // $this->Picter();
        move_uploaded_file($this->fileTmpName, "images/media/".$dir."/".$this->fileName);
    }*/
    
    function createNewImage($pic, $dir, $newname) {
        // получение новых размеров
        $this->ratio_orig = $pic->origWidth/$pic->origHeight;
        if ($pic->minWidth/$pic->minHeight > $this->ratio_orig) {
            $pic->minWidth = $pic->minHeight * $this->ratio_orig;
            $pic->minWidth2 = $pic->minHeight2 * $this->ratio_orig;
        } else {
            $pic->minHeight = $pic->minWidth / $this->ratio_orig;
            $pic->minHeight2 = $pic->minWidth2 / $this->ratio_orig;
        }
        // ресэмплирование
        $this->image_p = imagecreatetruecolor($pic->minWidth, $pic->minHeight);
        imagefilledrectangle($this->image_p, 0, 0, 720, 1080, imagecolorallocate($this->image_p, 234, 234, 235));
        $this->image_p2 = imagecreatetruecolor(720, 515);
        imagefilledrectangle($this->image_p2, 0, 0, 720, 515, imagecolorallocate($this->image_p2, 234, 234, 235));
        if ($pic->fileFormat==1) {$this->image = imagecreatefromgif($pic->fileTmpName);$ext='gif';}
        if ($pic->fileFormat==2) {$this->image = imagecreatefromjpeg($pic->fileTmpName);$ext='jpg';}
        if ($pic->fileFormat==3) {$this->image = imagecreatefrompng($pic->fileTmpName);$ext='png';}
        
        imagecopyresampled($this->image_p, $this->image, 0, 0, 0, 0, $pic->minWidth, $pic->minHeight, $pic->origWidth, $pic->origHeight);        
        imagecopyresampled($this->image_p2,  $this->image, (720-$pic->minWidth)/2, (515-$pic->minHeight)/2, 0, 0, $pic->minWidth, $pic->minHeight, $pic->origWidth, $pic->origHeight);
        
        // обрезание :)
        //$this->image_p2 = imagecreatetruecolor(640, 480);
        //imagefilledrectangle($this->image_p2, 0, 0, 640, 480, imagecolorallocate($this->image_p2, 234, 234, 235));
        //imagecopy($this->image_p2, $this->image_p, 0,0, ($this->minWidth-640)/2, ($this->minHeight-480)/2, 640, 480);
       /* $this->image_p2 = imagecreatetruecolor(640, 480);
        imagefilledrectangle($this->image_p2, 0, 0, 640, 480, imagecolorallocate($this->image_p2, 234, 234, 235));
        imagecopyresampled($this->image_p2, $this->image_p2, 0, 0, 0, 0, 640, 480, 640, 480);
        imagecopy($this->image_p2, $this->image_p,  (640-$pic->minWidth)/2, (480-$pic->minHeight)/2,0,0, $pic->minWidth, $pic->minHeight);*/
        
        // рисование
        //imagejpeg($image_p2, null, 100);
        move_uploaded_file($pic->fileTmpName, "../images/diplom/".$newname);
        imagejpeg($this->image_p2, "../images/diplom/thumb/".$newname, 70);
    }
}
?>
