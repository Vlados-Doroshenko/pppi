<?php if (isset($_GET['editStudent'])) { ?>
    <script> $(document).ready(function () {
            $('#editStudent').modal('show');
        });</script>
<?php } ?>

<?php
if (isset($_POST['editStudent'])) {
    $name = (isset($_POST['name_student'])) ? mysqli_real_escape_string($db, $_POST['name_student']) : '';
    $email = (isset($_POST['email_student'])) ? mysqli_real_escape_string($db, $_POST['email_student']) : '';
    $phone = (isset($_POST['phone_student'])) ? mysqli_real_escape_string($db, $_POST['phone_student']) : '';
    $specialty = (isset($_POST['id_specialty'])) ? mysqli_real_escape_string($db, $_POST['id_specialty']) : '';
    $group = (isset($_POST['id_group'])) ? mysqli_real_escape_string($db, $_POST['id_group']) : '';
    $faculty = (isset($_POST['id_faculty'])) ? mysqli_real_escape_string($db, $_POST['id_faculty']) : '';

    mysqli_query($db, "UPDATE `students`
    SET
    name_student = '{$name}',
    email_student = '{$email}',
    phone_student = '{$phone}',
    idGroup_student = '{$group}',
    idFaculty_student = '{$faculty}',
    idSpecialty_student = '{$specialty}'
    WHERE id_student='".$_GET['editStudent']."' ");
    
    echo "<script> document.location.href='?students'; </script>";
}
$editStudent = mysqli_fetch_assoc(mysqli_query($db, "SELECT * FROM students, specialty, groups, faculties WHERE id_group = idGroup_student AND id_faculty = idFaculty_student AND id_specialty = idSpecialty_student AND id_student='".$_GET['editStudent']."'"));
?>
<div class="modal fade" id="editStudent" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" title='Редагування "<?php echo $editStudent['name_student'] ?>" (<?php echo $_GET['editStudent']; ?>)'><i class="fas fa-user-graduate fa-lg"></i> Редагування "<?php echo $editStudent['name_student'] ?>" (<?php echo $_GET['editStudent']; ?>)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post">
                    <div class="input-group mb-3" title="<?php echo $editStudent['name_group']; ?>">
                        <input class="form-control" type="text" name="name_student" value="<?= $editStudent['name_student']; ?>"/>
                    </div>
                    <div class="input-group mb-3" title="<?php echo $editStudent['email_student']; ?>">
                        <input class="form-control" type="text" name="email_student" value="<?= $editStudent['email_student']; ?>"/>
                    </div>
                    <div class="input-group mb-3" title="<?php echo $editStudent['phone_student']; ?>">
                        <input class="form-control" type="text" name="phone_student" value="<?= $editStudent['phone_student']; ?>"/>
                    </div>
                    <div class="input-group mb-3" title="<?php echo $editStudent['name_group']; ?>">
                        <?php $query_group = mysqli_query($db, "SELECT * FROM groups"); ?>
                        <select class="form-control" name="id_group" style="width: 100%;">
                            <?php
                            while ($group = mysqli_fetch_assoc($query_group)) {
                                if ($editStudent['idGroup_student'] == $group['id_group']) {
                                    echo '<OPTION value="' . $group['id_group'] . '" SELECTED>' . $group['name_group'] . '</OPTION>';
                                } else {
                                    echo '<OPTION value="' . $group['id_group'] . '">' . $group['name_group'] . '</OPTION>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="input-group mb-3" title="<?php echo $editStudent['name_faculty']; ?>">
                        <?php $query_faculties= mysqli_query($db, "SELECT * FROM faculties"); ?>
                        <select class="form-control" name="id_faculty" style="width: 100%;">
                            <?php
                            while ($faculties = mysqli_fetch_assoc($query_faculties)) {
                                if ($editStudent['idGroup_student'] == $faculties['id_faculty']) {
                                    echo '<OPTION value="' . $faculties['id_faculty'] . '" SELECTED>' . $faculties['name_faculty'] . '</OPTION>';
                                } else {
                                    echo '<OPTION value="' . $faculties['id_faculty'] . '">' . $faculties['name_faculty'] . '</OPTION>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <div class="input-group mb-3" title="<?php echo $editStudent['name_specialty']; ?>">
                        <?php $query_specialty = mysqli_query($db, "SELECT * FROM specialty"); ?>
                        <select class="form-control" name="id_specialty" style="width: 100%;">
                            <?php
                            while ($specialty = mysqli_fetch_assoc($query_specialty)) {
                                if ($editStudent['idGroup_student'] == $specialty['id_specialty']) {
                                    echo '<OPTION value="' . $specialty['id_specialty'] . '" SELECTED>' . $specialty['name_specialty'] . '</OPTION>';
                                } else {
                                    echo '<OPTION value="' . $specialty['id_specialty'] . '">' . $specialty['name_specialty'] . '</OPTION>';
                                }
                            }
                            ?>
                        </select>
                    </div>
                    <hr>
                    <button class="btn btn-info btn-block" title="Зберегти" type="submit" name="editStudent"><i class="far fa-save fa-lg"></i> Зберегти</button>              
                </form>
            </div>         
        </div>
    </div>
</div>