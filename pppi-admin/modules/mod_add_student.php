<?php if (isset($_GET['addStudent'])) { ?>
    <script> $(document).ready(function () {
            $('#addStudent').modal('show');
        });</script>
<?php } ?>

<?php
if (isset($_POST['addStudent'])) {

    $name = (isset($_POST['name_student'])) ? mysqli_real_escape_string($db, $_POST['name_student']) : '';
    $email = (isset($_POST['email_student'])) ? mysqli_real_escape_string($db, $_POST['email_student']) : '';
    $phone = (isset($_POST['phone_student'])) ? mysqli_real_escape_string($db, $_POST['phone_student']) : '';
    $specialty = (isset($_POST['id_specialty'])) ? mysqli_real_escape_string($db, $_POST['id_specialty']) : '';
    $group = (isset($_POST['id_group'])) ? mysqli_real_escape_string($db, $_POST['id_group']) : '';
    $faculty = (isset($_POST['id_faculty'])) ? mysqli_real_escape_string($db, $_POST['id_faculty']) : '';

    mysqli_query($db, "INSERT  INTO students
        SET
        name_student = '{$name}',
        email_student = '{$email}',
        phone_student = '{$phone}',
        idGroup_student = '{$group}',
        idFaculty_student = '{$faculty}',
        idSpecialty_student = '{$specialty}' ");
    echo "<script>$.confirm({title: 'Успішно!', content: 'Студент, {$name}, доданий', buttons: { OK: function() { document.location.href='?students';}} });</script>";
}
?>
<div class="modal fade" id="addStudent" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" title="Додати студента"><i
                            class="fas fa-user-graduate fa-lg"></i> Додати студента</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post">
                    <div class="input-group mb-3" title="ПІП">
                        <input class="form-control" type="text" name="name_student"
                               value="" placeholder="ПІП" required/>
                    </div>
                    <div class="input-group mb-3" title="Email">
                        <input class="form-control" type="text" name="email_student"
                               value="" placeholder="Email" required/>
                    </div>
                    <div class="input-group mb-3" title="Телефон">
                        <input class="form-control" type="text" name="phone_student"
                               value="" placeholder="Телефон" required/>
                    </div>
                    <div class="input-group mb-3" title="Група">
                        <?php $query_categ = mysqli_query($db, "SELECT * FROM groups"); ?>
                        <select class="form-control" name="id_group" style="width: 100%;" required>
                            <option value="">Оберіть групу</option>
                            <?php
                            while ($categ = mysqli_fetch_assoc($query_categ)) {
                                echo '<OPTION value="' . $categ['id_group'] . '">' . $categ['name_group'] . '</OPTION>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="input-group mb-3" title="Факультет">
                        <?php $query_faculties = mysqli_query($db, "SELECT * FROM faculties"); ?>
                        <select class="form-control" name="id_faculty" style="width: 100%;" required>
                            <option value="">Оберіть факультет</option>
                            <?php
                            while ($faculties = mysqli_fetch_assoc($query_faculties)) {
                                echo '<OPTION value="' . $faculties['id_faculty'] . '">' . $faculties['name_faculty'] . '</OPTION>';
                            }
                            ?>
                        </select>
                    </div>
                    <div class="input-group mb-3" title="Спеціальність">
                        <?php $query_specialty = mysqli_query($db, "SELECT * FROM specialty"); ?>
                        <select class="form-control" name="id_specialty" style="width: 100%;" required>
                            <option value="">Оберіть спеціальність</option>
                            <?php
                            while ($specialty = mysqli_fetch_assoc($query_specialty)) {
                                echo '<OPTION value="' . $specialty['id_specialty'] . '">' . $specialty['name_specialty'] . '</OPTION>';
                            }
                            ?>
                        </select>
                    </div>
                    <hr>
                    <button class="btn btn-info btn-block" title="Додати" type="submit" name="addStudent"><i
                                class="fas fa-folder-plus"></i> Додати
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>