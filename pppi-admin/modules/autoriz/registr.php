<?php
if (isset($_POST['addRieltor'])) {

    function GenerateSalt($n = 3) {
        $key = '';
        $pattern = '1234567890bcdefghijklmnopqrstuvwxyz.,*_-+';
        $counter = strlen($pattern) - 1;
        for ($i = 0; $i < $n; $i++) {
            $key .= $pattern{rand(0, $counter)};
        }
        return $key;
    }

    $error = "";
    $login = (isset($_POST['login'])) ? mysqli_real_escape_string($db, $_POST['login']) : '';
    $pib = (isset($_POST['pib'])) ? mysqli_real_escape_string($db, $_POST['pib']) : '';
    $password = (isset($_POST['password'])) ? mysqli_real_escape_string($db, $_POST['password']) : '';
    $phones = (isset($_POST['phones'])) ? mysqli_real_escape_string($db, $_POST['phones']) : '';
    $email = (isset($_POST['email'])) ? mysqli_real_escape_string($db, $_POST['email']) : '';

    $queryLog = mysqli_query($db, "select * from users where login_user='{$login}'");
    if (mysqli_num_rows($queryLog) >= 1) {
        //echo "<script>alert('Такой логин уже существует')</script>";
        $error .= 'Такий login зайнятий </br>';
    }

    $phone = mysqli_query($db, "SELECT phone_user FROM users WHERE phone_user = '{$phones}'");
    if (mysqli_num_rows($phone) >= 1) {
        $error.= "Такий номер телефону вже є </br>";
    }

    $mail = mysqli_query($db, "SELECT email_user FROM users WHERE email_user = '{$email}'");
    if (mysqli_num_rows($mail) >= 1) {
        $error.= "Такий E-mail вже є </br>";
    }


    if ($error == "") {
        $salt = GenerateSalt();
        $hashed_password = md5(md5($password) . $salt);
        mysqli_query($db, "INSERT  INTO `users`
            SET
            name_user = '{$pib}',
            login_user = '{$login}',
            password_user = '{$hashed_password}',
            salt = '{$salt}',
            email_user = '{$email}',
            phone_user = '{$phones}',
            idCateg_user = '2' ");
        echo "<script>$.confirm({title: 'Успішно!', content: 'Користувач {$pib} зарєєстрований', buttons: { OK: function() { document.location.href='./';}} });</script>";
        //echo "<script>document.location.href='./';</script>";
    } else {
        echo "<script>$.confirm({title: 'Помилка!', content: '". $error ."', buttons: { OK: function() { document.location.href=document.location.href;}} });</script>";
    }
}
?>
<div class="container">
    <div class="row">
        <div class="text-center form-log">
            <div class="d-inline-block"><img src="logo.png" width="70px;" alt="Logo"/></div>
            <hr/>
            <h2 class="text-center">Реєстрація</h2>
            <form action="" method="post">
                <div class="input-group mb-3" title="ПІП">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-user"></i></span>
                    </div>
                    <input type="text" class="form-control" name="pib" placeholder="ПІП*" required>
                </div>
                <div class="input-group mb-3" title="Login">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-sign-in-alt"></i></span>
                    </div>
                    <input type="text" class="form-control" name="login" placeholder="Login (мін.3 символа)*" minlength="3" required>
                </div>
                <div class="input-group mb-3" title="Пароль">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-key"></i></span>
                    </div>
                    <input type="password" class="form-control" name="password" placeholder="Пароль (мін.3 символа)*" minlength="3" required>
                </div>
                <div class="input-group mb-3" title="E-mail">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                    </div>
                    <input type="text" class="form-control" name="email" placeholder="E-mail*" required>
                </div>
                <div class="input-group mb-3" title="Телефон">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-phone-alt"></i></span>
                    </div>
                    <input type="text" class="form-control" placeholder="Телефон (097xxxxxxx)*" pattern="0[0-9]{2}[0-9]{3}[0-9]{4}" name="phones" required>
                </div>
                <hr>
                <div class="btn-group btn-block" role="group">
                    <button class="btn btn-info" title="Зареєструватися" type="submit" name="addRieltor"><i class="fas fa-user-plus"></i> Зареєструватися</button>
                    <button onclick="history.back()" class="btn btn-outline-info" type="button" title="Скасувати"><i class="fas fa-times"></i> Скасувати</button>
                </div>
            </form>
        </div>
    </div>
</div>