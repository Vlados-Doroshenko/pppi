<?php
/* session_start(); */
if (isset($_POST['log'])) {
  $login = (isset($_POST['login'])) ? mysqli_real_escape_string($db, $_POST['login']) : '';
  $password = (isset($_POST['password'])) ? mysqli_real_escape_string($db, $_POST['password']) : '';


  $queryLog = mysqli_query($db, "SELECT * FROM users WHERE login_user='{$login}' LIMIT 1");

  if (mysqli_num_rows($queryLog) == 1) {
    $log = mysqli_fetch_assoc($queryLog);

    $pass = md5(md5($password) . $log['salt']);


    if ($log['password_user'] == $pass) {
      if ($log['idCateg_user'] !== '1') {
        echo "<script>$.confirm({title: 'Error!', content: 'Ваш профіль обмежений по правам', buttons: { OK: function() { document.location.href='.././';}} });</script>";
      } else {
        $_SESSION['user_admin'] = $log['id_user'];
        $_SESSION['user_fio_admin'] = $log['name_user'];
        $_SESSION['user_email_admin'] = $log['email_user'];
        $_SESSION['user_phones_admin'] = $log['phone_user'];
        $_SESSION['userCateg_admin'] = $log['idCateg_user'];
        $date = date("Y-m-d H:i:s");
        echo "<script>$.confirm({title: 'Успішно!', content: 'Вітаю, " . $log['name_user'] . ", Ви авторизувалися!', buttons: { OK: function() { document.location.href='./';}} });</script>";
      }
    } else {
      echo "<script>$.confirm({title: 'Помилка!', content: 'Некоректний ввід паролю', buttons: { OK: function() { document.location.href=document.location.href;}} });</script>";
    }
  } else {
    echo "<script>$.confirm({title: 'Помилка!', content: 'Такого логіну немає', buttons: { OK: function() { document.location.href=document.location.href;}} });</script>";
  }
}
?>
<div class="container">
  <div class="row">
    <div class="text-center form-log">
      <div class="d-inline-block"><img src="../src/images/favicon.png" width="170px;" alt="Logo"/></div>
      <hr/>
      <h2 class="text-center">Вхід</h2>
      <form method="post">
        <div class="input-group mb-3" title="Login">
          <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-sign-in-alt"></i></span>
          </div>
          <input type="text" class="form-control" name="login" placeholder="Login (мін.3 символа)*" minlength="3"
                 required>
        </div>
        <div class="input-group mb-3" title="Пароль">
          <div class="input-group-prepend">
            <span class="input-group-text"><i class="fas fa-key"></i></span>
          </div>
          <input type="password" class="form-control" name="password" placeholder="Пароль (мін.3 символа)*"
                 minlength="3" required>
        </div>
        <hr>
        <div class="btn-group btn-block" role="group">
          <button class="btn btn-info" title="Ввійти" type="submit" name="log"><i class="fas fa-sign-in-alt"></i> Ввійти
          </button>
          <a class="btn btn-outline-info" title="Реєстрація" href="?reg"><i class="fas fa-user-plus"></i> Реєстрація</a>
        </div>
      </form>
    </div>
  </div>
</div>

