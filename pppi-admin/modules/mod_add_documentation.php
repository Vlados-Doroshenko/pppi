<?php if (isset($_GET['addDoc'])) { ?>
    <script> $(document).ready(function () {
            $('#addDoc').modal('show');
        });</script>
<?php } ?>

<?php
if (isset($_POST['addDoc'])) {

    $schedule = (isset($_POST['schedule_setting'])) ? mysqli_real_escape_string($db, $_POST['schedule_setting']) : '';
    $rating = (isset($_POST['rating_setting'])) ? mysqli_real_escape_string($db, $_POST['rating_setting']) : '';
    $scholarship = (isset($_POST['scholarship_setting'])) ? mysqli_real_escape_string($db, $_POST['scholarship_setting']) : '';

    mysqli_query($db, "INSERT  INTO settings
        SET
        schedule_setting = '{$schedule}', 
        rating_setting = '{$rating}', 
        scholarship_setting = '{$scholarship}' ");
    echo "<script> document.location.href='?documentation';</script>";
}
?>
<div class="modal fade" id="addDoc" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" title="Додати документи"><i class="fa fa-file fa-lg"></i>
                    Додати документи</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post">
                    <div class="input-group mb-3" title="Посилання на розклад">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-signature"></i></span>
                        </div>
                        <input type="text" class="form-control" name="schedule_setting" placeholder="Посилання на розклад*"
                               autocomplete="off" required>
                    </div>
                    <div class="input-group mb-3" title="Посилання на рейтинг">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-signature"></i></span>
                        </div>
                        <input type="text" class="form-control" name="rating_setting" placeholder="Посилання на рейтинг*"
                               autocomplete="off" required>
                    </div>
                    <div class="input-group mb-3" title="Посилання на стипендію">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-signature"></i></span>
                        </div>
                        <input type="text" class="form-control" name="scholarship_setting"
                               placeholder="Посилання на стипендію*"
                               autocomplete="off" required>
                    </div>
                    <hr>
                    <button class="btn btn-info btn-block" title="Додати" type="submit" name="addDoc"><i
                                class="fas fa-folder-plus"></i> Додати
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>