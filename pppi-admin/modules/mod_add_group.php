<?php if (isset($_GET['addGroup'])) { ?>
    <script> $(document).ready(function () {
            $('#addGroup').modal('show');
        });</script>
<?php } ?>

<?php
if (isset($_POST['addGroup'])) {

    $name = (isset($_POST['name_group'])) ? mysqli_real_escape_string($db, $_POST['name_group']) : '';

    mysqli_query($db, "INSERT  INTO groups
        SET
        name_group = '{$name}' ");
    echo "<script>$.confirm({title: 'Успішно!', content: 'Група {$name} додана', buttons: { OK: function() { document.location.href='?groups';}} });</script>";
}
?>
<div class="modal fade" id="addGroup" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" title="Додати групу"><i
                            class="fa fa-object-group fa-lg"></i> Додати групу</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post">
                    <div class="input-group mb-3" title="Назва групи">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-signature"></i></span>
                        </div>
                        <input type="text" class="form-control" name="name_group" placeholder="Назва групи*"
                               autocomplete="off" required>
                    </div>
                    <hr>
                    <button class="btn btn-info btn-block" title="Додати" type="submit" name="addGroup"><i
                                class="fas fa-folder-plus"></i> Додати
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>