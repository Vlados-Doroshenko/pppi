<?php session_start();
include '../../config.php';
$id=$_POST['id'];
$salt=$_POST['salt'];
$pib=$_POST['pib'];
?>
<div class="modal fade" id="editPass" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" title="Зміна пароля: <?php echo $pib; ?>"><i class="fas fa-lock fa-lg"></i> Зміна пароля: <?php echo $pib; ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="input-group mb-3" title="Новий пароль">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="text" class="form-control inpPass" placeholder="Новий пароль (мін.3 символа)*" minlength="3" required>
                    </div>
                    <hr/>
                    <button class="btn btn-info btn-block SavePass" title="Зберегти" data-salt="<?php echo $salt; ?>" data-pib="<?php echo $pib; ?>" data-pk="<?php echo $id; ?>" type="submit"><i class="fas fa-save fa-lg"></i> Зберегти</button>
                </form>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).on("click","button.SavePass", function(){
        var npq=$('input.inpPass').val();
        var id=$(this).attr('data-pk');
        var salt=$(this).attr('data-salt');
        pib=$(this).attr('data-pib');
        var dataNP="id="+id+"&salt="+salt+"&np="+npq;
        //alert(npq+' - '+dataNP);
        $.post("modules/aj_edit_pass.php",dataNP,succ_aj_edNP);
    });
    function succ_aj_edNP(ajRequestNP) {
        alert(pib+' успішно змінив пароль');
        document.location.href=location.href;
    };
</script>