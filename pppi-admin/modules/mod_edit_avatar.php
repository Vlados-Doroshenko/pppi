<?php if (isset($_GET['editAvatar'])) { ?>
    <script> $(document).ready(function () {
            $('#editAvatar').modal('show');
        });</script>
<?php } ?>
<?php
if (isset($_POST['saveAv'])) {
        $dir = $_GET['editAvatar'];
        include 'modules/picter_av.php';
        include 'modules/resemp_av.php';
        //@mkdir("pic/avatar/".$dir, 0777);
        $dirNew = "../../images/avatar/".$dir."/";
        //$fots = array_slice(scandir($dirNew), 2);
        //$fots = glob($dirNew."*.jpg");
        $fots = glob("../../images/avatar/" . $dir . "/*.jpg");
        natsort($fots);
        $fil=array_pop($fots);
        //print "<script> alert('".$fil."'); </script>";
        $fil1 = explode('/',$fil);
        $fil=array_pop($fil1);
        $fill = explode('.',$fil);
        $count = 0;
        $count = $fill[0];

        foreach($_FILES["filename"]["name"] as $k=>$v) {
            $count++;
            $newname = $count;

            $pic = new Picter();
            $newPic = new Resemp($pic);
            $pic->ustanovka($k);
            $newPic->createNewImage($pic, $dir, $newname);
            unset($pic);
            unset($newPic);
        }
    if (isset($_GET['all_user'])) {
        print "<script> document.location.href ='?all_user';</script>";
    }

}
?>
<div class="modal fade" id="editAvatar" tabindex="-1" role="dialog" data-backdrop="static" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" title="Виберіть фото (ID: <?php echo $_GET['editAvatar']; ?>)"><i class="fas fa-image fa-lg"></i> Виберіть фото (ID: <?php echo $_GET['editAvatar']; ?>):</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="custom-file" title="Виберіть файл">
                        <input type="file" name="filename[]" class="custom-file-input" id="customFileLang-<?php echo $_GET['editAvatar']; ?>" onchange='document.querySelector(".custom-file-label").innerHTML = Array.from(this.files).map(f => f.name).join("<br />")'>
                        <label class="custom-file-label" for="customFileLang-<?php echo $_GET['editAvatar']; ?>" data-browse="Огляд">Виберіть файл</label>
                    </div>
                    <hr/>
                    <button class="btn btn-info btn-block" title="Зберегти" name="saveAv" type="submit"><i class="fas fa-save fa-lg"></i> Зберегти</button>
                </form>

            </div>
        </div>
    </div>
</div>