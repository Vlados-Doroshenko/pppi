<?php if (isset($_GET['addUser'])) { ?>
    <script> $(document).ready(function () {
            $('#addUser').modal('show');
        });</script>
<?php } ?>

<?php
if (isset($_POST['addUser'])) {

    function GenerateSalt($n = 3) {
        $key = '';
        $pattern = '1234567890bcdefghijklmnopqrstuvwxyz.,*_-+';
        $counter = strlen($pattern) - 1;
        for ($i = 0; $i < $n; $i++) {
            $key .= $pattern{rand(0, $counter)};
        }
        return $key;
    }

    $error = "";
    $login = (isset($_POST['login'])) ? mysqli_real_escape_string($db, $_POST['login']) : '';
//    $nickname = (isset($_POST['nickname'])) ? mysqli_real_escape_string($db, $_POST['nickname']) : '';
//    $profession = (isset($_POST['profession'])) ? mysqli_real_escape_string($db, $_POST['profession']) : '';
    $pib = (isset($_POST['name'])) ? mysqli_real_escape_string($db, $_POST['name']) : '';
    $password = (isset($_POST['password'])) ? mysqli_real_escape_string($db, $_POST['password']) : '';
    $phones = (isset($_POST['phone'])) ? mysqli_real_escape_string($db, $_POST['phone']) : '';
    $email = (isset($_POST['email'])) ? mysqli_real_escape_string($db, $_POST['email']) : '';

    $queryLog = mysqli_query($db, "select * from users where login_user='{$login}'");
    if (mysqli_num_rows($queryLog) >= 1) {
        //echo "<script>alert('Такой логин уже существует')</script>";
        $error .= 'Такий login зайнятий <br/>';
    }
//    $queryNick = mysqli_query($db, "select * from user where nickname='{$nickname}'");
//    if (mysqli_num_rows($queryNick) >= 1) {
//        //echo "<script>alert('Такой логин уже существует')</script>";
//        $error .= 'Такий nickname зайнятий <br/>';
//    }

    $phone = mysqli_query($db, "SELECT phone_user FROM users WHERE phone_user = '{$phones}'");
    if (mysqli_num_rows($phone) >= 1) {
        $error.= "Такий номер телефону вже є <br/>";
    }

    $mail = mysqli_query($db, "SELECT email_user FROM users WHERE email_user = '{$email}'");
    if (mysqli_num_rows($mail) >= 1) {
        $error.= "Такий E-mail вже є <br/>";
    }

    if ($error == "") {
        $salt = GenerateSalt();
        $hashed_password = md5(md5($password) . $salt);
        mysqli_query($db, "INSERT  INTO `users`
            SET
            name_user = '{$pib}',
            login_user = '{$login}',
            password_user = '{$hashed_password}',
            salt = '{$salt}',
            phone_user = '{$phones}',
            email_user = '{$email}',
            idCateg_user = '2' ");
        echo "<script>$.confirm({title: 'Успішно!', content: 'Користувач {$pib} зарєєстрований', buttons: { OK: function() { document.location.href='?all_user';}} });</script>";
    } else {
        echo "<script>$.confirm({title: 'Помилка!', content: '". $error ."', buttons: { OK: function() { document.location.href=document.location.href;}} });</script>";
    }
}
?>
<div class="modal fade" id="addUser" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" title="Створення нового користувача"><i class="fas fa-user-plus fa-lg"></i> Створення нового користувача</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="" method="post">
                    <div class="input-group mb-3" title="ПІП">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-user"></i></span>
                        </div>
                        <input type="text" class="form-control" name="name" placeholder="ПІП*" autocomplete="off" required>
                    </div>
                    <div class="input-group mb-3" title="Login">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-sign-in-alt"></i></span>
                        </div>
                        <input type="text" class="form-control" name="login" placeholder="Login (мін.3 символа)*" minlength="3" autocomplete="off" required>
                    </div>
                    <div class="input-group mb-3" title="Пароль">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-key"></i></span>
                        </div>
                        <input type="text" class="form-control" name="password" placeholder="Пароль (мін.3 символа)*" minlength="3" autocomplete="off" required>
                    </div>
                    <div class="input-group mb-3" title="E-mail">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                        </div>
                        <input type="text" class="form-control" name="email" placeholder="E-mail*" autocomplete="off" required>
                    </div>
                    <div class="input-group mb-3" title="Телефон">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-phone-alt"></i></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Телефон (+38097xxxxxxx)*" pattern="+380[0-9]{2}[0-9]{3}[0-9]{4}" name="phone" autocomplete="off" required>
                    </div>
                    <hr>
                    <button class="btn btn-info btn-block" title="Зареєструвати" type="submit" name="addUser"><i class="fas fa-user-plus fa-lg"></i> Зареєструвати</button>
                </form>
            </div>
        </div>
    </div>
</div>