<?php
if (isset($_GET['delDoc'])) {
    $blc = mysqli_query($db, "DELETE FROM settings WHERE id_setting ='" . $_GET['delDoc'] . "'");
    echo "<script> document.location.href='?documentation'; </script>";
}
$sql = "SELECT * FROM settings";
?>
<div class="container">
    <div class="title-table">
        <h3 class="text-center" title='Документи'><i class="fa fa-file fa-lg"></i> Документи</h3>
    </div>
    <div class="row">
        <div class="shapka" id="toolbar1">
            <button id="show" title="Залишити відмічені" class="btn btn-secondary">
                <i class="far fa-check-square fa-lg"></i>
            </button>
            <a title="Додати документи" class="btn btn-secondary addRiel" href="?documentation&addDoc">
                <i class="fa fa-file fa-lg"></i> Додати документи
            </a>
            <a title="Очистити сортування" class="btn btn-secondary" href="?documentation">
                <i class="fas fa-times fa-lg"></i>
            </a>
        </div>
        <div id="table1" class="table-responsive">
            <table class="table table-striped table-condensed table-hover"
                   data-locale="uk-UA"
                   id="table111"
                   data-toggle="table111"
                   data-show-toggle="false"
                   data-toolbar="#toolbar1"
                   data-show-fullscreen="false"
                   data-filter-control="true"
                   data-filter-show-clear="false"
                   data-show-print="false"
                   data-show-copy-rows="false"
                   data-show-export="false"
                   data-click-to-select="false"
                   data-pagination="true"
                   data-page-list="[10, 25, 50, 100, 250, 500]"
                   data-maintain-selected="true"
                   data-maintain-meta-data="true"
                   data-show-refresh="false"
                   data-show-columns="true"
                   data-show-search-button="false"
                   data-show-search-clear-button="true"
                   data-unique-id="id_a_n"
                   data-minimum-count-columns="1"
                   data-detail-view="false"
                   data-mobile-responsive="true"
                   data-check-on-init="true" '>
                <thead>
                <tr>
                    <th data-field="state" data-print-ignore="true" data-checkbox="true" tabindex="0"></th>
                    <th class="idd text-center" data-sortable="true" title="ID" data-filter-control="input"
                        data-visible="true" data-field="id">ID
                    </th>
                    <th data-sortable="true" title="Розклад" data-filter-control="input" data-visible="true"
                        data-field="schedule_setting">Розклад
                    </th>
                    <th data-sortable="true" title="Рейтинг" data-filter-control="input" data-visible="true"
                        data-field="rating_setting">Рейтинг
                    </th>
                    <th data-sortable="true" title="Стипендія" data-filter-control="input" data-visible="true"
                        data-field="scholarship_setting">Стипендія
                    </th>
                    <th class="text-center" title="Видалити" data-print-ignore="true">Видалити</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $queryBlock = mysqli_query($db, $sql);
                while ($blk = mysqli_fetch_assoc($queryBlock)) {
                    ?>
                    <tr id="tr-id-<?php echo $blk['id_setting']; ?>" class="tr-c-<?php echo $blk['id_setting']; ?>">
                        <td id="td-id-<?php echo $blk['id_setting']; ?>"
                            class="td-c-<?php echo $blk['id_setting']; ?>"></td>
                        <td title="<?php echo $blk['id_setting']; ?>"><?php echo $blk['id_setting']; ?></td>
                        <td title="<?php echo $blk['name_setting']; ?>">
                            <span data-type="text" data-title="Розклад" data-mode="popup" data-placement="top"
                                  data-pk="<?php echo $blk['id_setting']; ?>" data-name="schedule_setting"
                                  class="editName"> <?php echo $blk['schedule_setting']; ?> </span>
                        </td>
                        <td title="<?php echo $blk['name_setting']; ?>">
                            <span data-type="text" data-title="Рейтинг" data-mode="popup" data-placement="top"
                                  data-pk="<?php echo $blk['id_setting']; ?>" data-name="rating_setting"
                                  class="editName"> <?php echo $blk['rating_setting']; ?> </span>
                        </td>
                        <td title="<?php echo $blk['name_setting']; ?>">
                            <span data-type="text" data-title="Стипендія" data-mode="popup" data-placement="top"
                                  data-pk="<?php echo $blk['id_setting']; ?>" data-name="scholarship_setting"
                                  class="editName"> <?php echo $blk['scholarship_setting']; ?> </span>
                        </td>
                        <td title="Видалити">
                            <a data-toggle="tooltip" title="Видалити"
                               href="<?php echo "?documentation&delDoc=" . $blk['id_setting']; ?>"><i
                                    class="far fa-trash-alt"></i></a>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>

    </div>
</div>
<?php
if (isset($_GET['addDoc'])) {
    include 'modules/mod_add_documentation.php';
}
?>

<script>
    function myInit() {
        $(function () {
            $('.editName').editable({
                url: 'modules/aj_documentation.php',
                showbuttons: 'right',
                success: function (response, newValue) {
                    thiss = $(this);
                    thiss.html(newValue);
                    setTimeout(function () {
                        $('#table').bootstrapTable('updateCell', {
                            index: thiss.closest('tr').data('index'),
                            field: 'description',
                            value: thiss.closest('td').html()
                        });
                    }, 100);
                    thiss.closest('td').find('.popover').addClass('displayNone');
                    $('.tooltip').tooltip('destroy');
                    if ($(window).width() <= 400) {
                        setTimeout(function () {
                            $('#table').bootstrapTable('refresh', {silent: true});
                        }, 120);
                    }
                    setTimeout(function () {
                        myInit();
                    }, 150);
                }
            });
        });
    }

</script>
<script>
    $(document).ready(function () {

        $(function () {
            var $table1 = $('#table111'), selections1 = [], ids = [];

            function getHeight() {
                return $(window).height() - 180;
            }

            $(window).resize(function () {
                $table1.bootstrapTable('resetView', {'height': getHeight()});
            });

            $('#show').click(function () {
                $table1.bootstrapTable('togglePagination');
                $table1.bootstrapTable('checkInvert');
                var ids = $.map($table1.bootstrapTable('getSelections'), function (row) {
                    return row.id
                })
                $table1.bootstrapTable('remove', {
                    field: 'id',
                    values: ids
                })
                $table1.bootstrapTable('togglePagination');
            });

            $table1.bootstrapTable({
                height: getHeight(),
                silent: true,
                search: true,
                paginationLoop: true,
                sidePagination: 'client', // client or server
                totalRows: 1, // server side need to set
                pageNumber: 1,
                pageSize: 10,
                showPrint: true,
                paginationHAlign: 'right',
                paginationVAlign: 'both',
                icons: {print: 'fa-print', export: 'fa-file-export', columns: 'fa-list', clearSearch: 'fa-trash'}

            });
            setTimeout(function () {
                $table1.bootstrapTable('resetView', {'height': getHeight()});
            }, 1000);
            setTimeout(function () {
                myInit();
            }, 1100);
        });
    });
</script>

