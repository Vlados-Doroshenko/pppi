<?php session_start();
include '../../config.php';
$id=$_POST['id'];
$login=$_POST['log'];
$pib=$_POST['pib'];
?>
<div class="modal fade" id="editLog" tabindex="-1" data-backdrop="static" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" title="Зміна логіна: <?php echo $pib; ?>"><i class="fas fa-sign-in-alt"></i> Зміна логіна: <?php echo $pib; ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="input-group mb-3" title="Старий логін">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-sign-in-alt"></i></span>
                    </div>
                    <input type="text" class="form-control" value="<?php echo $login; ?>"  disabled="on">
                </div>
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="input-group mb-3" title="Новий логін">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fas fa-sign-in-alt"></i></span>
                        </div>
                        <input type="text" class="form-control inpLog" placeholder="Новий логін (мін.3 символа)*" minlength="3" required>
                    </div>
                    <hr/>
                    <button class="btn btn-info btn-block SaveLog" title="Зберегти" data-login="<?php echo $login; ?>" data-pib="<?php echo $pib; ?>" data-pk="<?php echo $id; ?>" type="submit"><i class="fas fa-save fa-lg"></i> Зберегти</button>
                </form>

            </div>
        </div>
    </div>
</div>
<script>
    $(document).on("click","button.SaveLog", function(){
        var nlog=$('input.inpLog').val();
        var id=$(this).attr('data-pk');
        var login=$(this).attr('data-login');
        pib=$(this).attr('data-pib');
        var dataNL="id="+id+"&log="+login+"&nl="+nlog;
        //alert(npq+' - '+dataNP);
        $.post("modules/aj_edit_login.php",dataNL,succ_aj_edNL);
    });
    function succ_aj_edNL(ajRequestNL) {
        alert(pib+' успішно змінив логін');
        document.location.href=location.href;
    };
</script>