<?php
if (isset($_POST['idCateg_user'])) {
  $Kat = mysqli_real_escape_string($db, $_POST['idCateg_user']);
  $idUser = mysqli_real_escape_string($db, $_POST['user_id']);
  mysqli_query($db, "UPDATE `users`, `users_category` SET
            idCateg_user = '{$Kat}'
            WHERE id_user = '{$idUser}' ");
  echo "<script> document.location.href='?all_user'; </script>";
}

if (isset($_GET['delUser'])) {
  $blc = mysqli_query($db, "DELETE FROM users WHERE id_user ='" . $_GET['delUser'] . "'");
  echo "<script> document.location.href='?all_user'; </script>";
}

$sql = "SELECT * FROM users, users_category WHERE id_category=idCateg_user";
?>
<div class="container">
  <div class="title-table">
    <h3 class="text-center" title='Користувачі'><i class="fas fa-users fa-lg"></i> Користувачі</h3>
  </div>
  <div class="row">
    <div class="shapka" id="toolbar1">
      <button id="show" title="Залишити відмічені" class="btn btn-secondary">
        <i class="far fa-check-square fa-lg"></i>
      </button>
      <a title="Створити користувача" class="btn btn-secondary addRiel" href="?all_user&addUser">
        <i class="fas fa-user-plus fa-lg"></i> Створити
      </a>
      <a title="Очистити сортування" class="btn btn-secondary" href="?all_user">
        <i class="fas fa-times fa-lg"></i>
      </a>
    </div>
    <div id="table1" class="table-responsive">
      <table class="table table-striped table-condensed table-hover"
             data-locale="uk-UA"
             id="table111"
             data-toggle="table111"
             data-show-toggle="false"
             data-toolbar="#toolbar1"
             data-show-fullscreen="false"
             data-filter-control="true"
             data-filter-show-clear="false"
             data-show-print="true"
             data-show-copy-rows="false"
             data-show-export="true"
             data-click-to-select="false"
             data-pagination="true"
             data-page-list="[10, 25, 50, 100, 250, 500]"
             data-maintain-selected="true"
             data-maintain-meta-data="true"
             data-show-refresh="false"
             data-show-columns="true"
             data-show-search-button="false"
             data-show-search-clear-button="true"
             data-unique-id="id"
             data-minimum-count-columns="1"
             data-detail-view="false"
             data-mobile-responsive="true"
             data-check-on-init="true"
             data-export-types="['excel', 'doc', 'pdf']"
             data-export-options='{ "fileName":"Користувачі", "worksheetName":"list1" }'>
        <thead>
        <tr>
          <th data-field="state" data-print-ignore="true" data-checkbox="true" tabindex="0"></th>
          <th class="idd text-center" data-sortable="true" title="ID" data-filter-control="input" data-visible="true"
              data-field="id">ID
          </th>
          <th data-sortable="true" class="text-center" title="ПІП" data-filter-control="input" data-visible="true"
              data-field="name">ПІП
          </th>
          <th class="text-center" title="Аватар" data-print-ignore="true">Аватар</th>
          <th data-sortable="true" class="text-center" title="Телефон" data-filter-control="input" data-visible="true"
              data-field="phone">Телефон
          </th>
          <th data-sortable="true" title="Права" data-print-ignore="true" data-visible="true" class="text-center"
              data-filter-control="input" data-field="user_kateg">Права
          </th>
          <th class="text-center" title="Дії" data-print-ignore="true">Дії</th>
          <th class="text-center" title="Видалити" data-print-ignore="true">Видалити</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $queryUser = mysqli_query($db, $sql);
        while ($user = mysqli_fetch_assoc($queryUser)) {
          ?>
          <tr id="tr-id-<?php echo $user['id_user']; ?>" class="tr-c-<?php echo $user['id_user']; ?>">
            <td id="td-id-<?php echo $user['id_user']; ?>" class="td-c-<?php echo $user['id_user']; ?>"></td>
            <td title="<?php echo $user['id_user']; ?>"><?php echo $user['id_user']; ?></td>
            <td title="<?php echo $user['name_user']; ?>">
              <span data-type="text" data-title="ПІП" data-mode="popup" data-placement="top"
                    data-pk="<?php echo $user['id_user']; ?>" data-name="name_user"
                    class="editPib"> <?php echo $user['name_user']; ?> </span>
            </td>
            <td title="Аватар">
                        <span class="edAv d-inline-block" title="Аватар" name="edAv">
                            <a href="index.php?all_user&editAvatar=<?php echo $user['id_user']; ?>">
                            <?php $i = 0;
                            foreach (glob("../src/images/avatar/" . $user['id_user'] . ".*") as $filename) {
                              if (file_exists($filename)) {
                                echo "<img width='45px' class='img-responsive' src='" . $filename . "?" . date('d:m:Y H:i:s') . "?" . $user['id_user'] . "'>";
                                $i++;
                              }
                            }
                            if ($i == 0) echo "<img width='45px' class='img-responsive' src='../src/images/noavatar.png'>";
                            ?>
                            </a>
                        </span>
            </td>
            <td title="<?php echo $user['phone_user']; ?>">
              <span data-type="text" data-title="Телефон" data-mode="popup" data-placement="top"
                    data-pk="<?php echo $user['id_user']; ?>" data-name="phone_user"
                    class="editPhones"> <?php echo $user['phone_user']; ?> </span>
            </td>
            <td title="<?php echo $user['name_category']; ?>">
              <form role="form" method="POST" action="">
                <select name="idCateg_user" class="form-control selColor" onchange="this.form.submit()">
                  <?php $queryKat = mysqli_query($db, "SELECT * FROM users_category");
                  while ($kat = mysqli_fetch_assoc($queryKat)) {
                    if ($user['idCateg_user'] == $kat['id_category']) {
                      ?>
                      <option value="<?php echo $kat['id_category']; ?>"
                              selected><?php echo $kat['name_category']; ?></option>
                    <?php } else { ?>
                      <option value="<?php echo $kat['id_category']; ?>"><?php echo $kat['name_category']; ?></option>
                    <?php }
                  } ?>
                </select>
                <input type="hidden" name="user_id" value="<?= $user['id_user']; ?>" />
              </form>
            </td>
            <td title="Дії">
              <div class="btn-group">
                <button type="button" class="btn btn-info btn-sm dropdown-toggle" title="Дії" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="fas fa-cogs"></i>
                </button>
                <div class="dropdown-menu">
                  <a class="dropdown-item btn edtLog" data-pk="<?php echo $user['id_user']; ?>" data-pib="<?php echo $user['name_user']; ?>" data-login="<?php echo $user['login_user']; ?>" title="Змінити логін"><i class="fas fa-sign-in-alt"></i> Змінити логін</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item btn edtPass" title="Змінити пароль" data-pk="<?php echo $user['id_user']; ?>" data-pib="<?php echo $user['name_user']; ?>" data-salt="<?php echo $user['salt']; ?>"><i class="fas fa-lock"></i> Змінити пароль</a>
                </div>
              </div>
            </td>
            <td title="Видалити">
              <a data-toggle="tooltip" title="Видалити" href="<?php echo "?all_user&delUser=".$user['id_user']; ?>"><i class="far fa-trash-alt"></i></a>
            </td>
          </tr>
        <?php } ?>
        </tbody>
      </table>
    </div>

  </div>
</div>
<?php
if (isset($_GET['editAvatar'])) {
  include 'modules/mod_edit_avatar.php';
}
if (isset($_GET['addUser'])) {
  include 'modules/mod_add_user.php';
}
?>
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("click", "a[data-type='pdf']", function () {
            var pdf = new jsPDF('p', 'pt', 'a4');
            $('table thead').css('color', '#000');
            pdf.addHTML($("#table111"), function () {
                pdf.save('Користувачі.pdf');
                document.location.href = document.location.href;
            });
        });
        $(document).on("click", ".activeSite", function () {
            $ActUs = $(this).find('input');
            var id = $ActUs.attr('data-pk');
            pib = $ActUs.attr('data-pib');
            var act = $ActUs.attr('data-active');
            var dataActUs = "id=" + id + "&act=" + act;
            $.post("modules/aj_active_site.php", dataActUs, succ_aj_EdActUS);
        });

        function succ_aj_EdActUS(ajReqActUS) {
            var actRes = ajReqActUS.split('|');
            $.alert("Сайт - " + actRes[1]);
            if (actRes[2] == 1) {
                $ActUs.attr('checked', true);
            } else {
                $ActUs.attr('checked', false);
            }
            $ActUs.attr('data-active', actRes[2]);
        };
        $(document).on("click", ".edtPass", function () {
            var id = $(this).attr('data-pk');
            var salt = $(this).attr('data-salt');
            var pib = $(this).attr('data-pib');
            var dataEdPass = "id=" + id + "&salt=" + salt + "&pib=" + pib;
            //alert(dataVd);
            $.post("modules/aj_mod_edit_pass.php", dataEdPass, succ_aj_edtPass);
        });

        function succ_aj_edtPass(ajRequestEdtPass) {
            $('#divModal').html(ajRequestEdtPass);
            $('#editPass').modal('show');
        };

        $(document).on("click", ".edtLog", function () {
            var id = $(this).attr('data-pk');
            var login = $(this).attr('data-login');
            var pib = $(this).attr('data-pib');
            var dataEdLog = "id=" + id + "&log=" + login + "&pib=" + pib;
            //alert(dataVd);
            $.post("modules/aj_mod_edit_login.php", dataEdLog, succ_aj_edtLog);
        });

        function succ_aj_edtLog(ajRequestEdtLogin) {
            $('#divModal').html(ajRequestEdtLogin);
            $('#editLog').modal('show');
        };
    });
</script>
<script>
    function myInit() {
        $(function () {
            $('.editLog').editable({
                url: 'modules/aj_nameklient.php',
                showbuttons: 'right',
                success: function (response, newValue) {
                    thiss = $(this);
                    thiss.html(newValue);
                    setTimeout(function () {
                        $('#table').bootstrapTable('updateCell', {
                            index: thiss.closest('tr').data('index'),
                            field: 'description',
                            value: thiss.closest('td').html()
                        });
                    }, 100);
                    thiss.closest('td').find('.popover').addClass('displayNone');
                    $('.tooltip').tooltip('destroy');
                    if ($(window).width() <= 400) {
                        setTimeout(function () {
                            $('#table').bootstrapTable('refresh', {silent: true});
                        }, 120);
                    }
                    setTimeout(function () {
                        myInit();
                    }, 150);
                    //alert(response+' || '+newValue);
                }
            });
            $('.editPib').editable({
                url: 'modules/aj_nameklient.php',
                showbuttons: 'right',
                success: function (response, newValue) {
                    thiss = $(this);
                    thiss.html(newValue);
                    setTimeout(function () {
                        $('#table').bootstrapTable('updateCell', {
                            index: thiss.closest('tr').data('index'),
                            field: 'description',
                            value: thiss.closest('td').html()
                        });
                    }, 100);
                    thiss.closest('td').find('.popover').addClass('displayNone');
                    $('.tooltip').tooltip('destroy');
                    if ($(window).width() <= 400) {
                        setTimeout(function () {
                            $('#table').bootstrapTable('refresh', {silent: true});
                        }, 120);
                    }
                    setTimeout(function () {
                        myInit();
                    }, 150);
                    //alert(response+' || '+newValue);
                }
            });
            $('.editPhones').editable({
                url: 'modules/aj_nameklient.php',
                showbuttons: 'right',
                success: function (response, newValue) {
                    thiss = $(this);
                    thiss.html(newValue);
                    setTimeout(function () {
                        $('#table').bootstrapTable('updateCell', {
                            index: thiss.closest('tr').data('index'),
                            field: 'description',
                            value: thiss.closest('td').html()
                        });
                    }, 100);
                    thiss.closest('td').find('.popover').addClass('displayNone');
                    $('.tooltip').tooltip('destroy');
                    if ($(window).width() <= 400) {
                        setTimeout(function () {
                            $('#table').bootstrapTable('refresh', {silent: true});
                        }, 120);
                    }
                    setTimeout(function () {
                        myInit();
                    }, 150);
                    //alert(response+' || '+newValue);
                }
            });
        });
    }

</script>
<script>
    $(document).ready(function () {

        $(function () {
            var $table1 = $('#table111'), selections1 = [], ids = [];

            function getHeight() {
                return $(window).height() - 350;
            }

            $(window).resize(function () {
                $table1.bootstrapTable('resetView', {'height': getHeight()});
            });

            /* function rowStyle(row, index) {
                 if (row.act == '0') {
                     return {classes: 'danger'}
                 } else {
                     if (row.act == '1') {
                         return {classes: 'active11'}
                     } else {
                         return {classes: 'toWork'}
                     }
                 }
             }*/

            $('#show').click(function () {
                $table1.bootstrapTable('togglePagination');
                $table1.bootstrapTable('checkInvert');
                /*alert('getSelections: ' + JSON.stringify($table1.bootstrapTable('getSelections')));*/
                var ids = $.map($table1.bootstrapTable('getSelections'), function (row) {
                    return row.id
                })
                $table1.bootstrapTable('remove', {
                    field: 'id',
                    values: ids
                })
                $table1.bootstrapTable('togglePagination');
            });

            $table1.bootstrapTable({
                height: getHeight(),
                //rowStyle: rowStyle,
                /*responseHandler: responseHandler,*/
                /*exportDataType: 'basic',*/
                silent: true,
                search: true,
                paginationLoop: true,
                sidePagination: 'client', // client or server
                totalRows: 1, // server side need to set
                pageNumber: 1,
                pageSize: 10,
                showPrint: true,
                //exportTypes: ['excel', 'doc', 'pdf'],
                paginationHAlign: 'right',
                paginationVAlign: 'both',
                icons: {print: 'fa-print', export: 'fa-file-export', columns: 'fa-list', clearSearch: 'fa-trash'}

            });
            setTimeout(function () {
                $table1.bootstrapTable('resetView', {'height': getHeight()});
            }, 1000);
            setTimeout(function () {
                myInit();
            }, 1100);
        });
    });
</script>

