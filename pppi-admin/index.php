<?php
header("Expires: Sat, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: post-check=0, pre-check=0", false);
session_cache_limiter("must-revalidate");
session_start();
include '../config.php';

if (isset($_GET['exit_admin'])) {
    unset($_SESSION['user_admin']);
    unset($_SESSION['user_fio_admin']);
    unset($_SESSION['user_email_admin']);
    unset($_SESSION['user_phones_admin']);
    unset($_SESSION['userCateg_admin']);
    echo "<script>location.href='./'</script>";
}

$sql = mysqli_fetch_assoc(mysqli_query($db, "SELECT * FROM users"));
?>

<!doctype html>
<html lang="uk">
<head>
    <title>Адмін панель</title>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" type="image/x-icon" href="../src/images/favicon.png"/>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/bt/bootstrap-table.min.css">
    <link rel="stylesheet" href="css/bt/bootstrap-editable.css">
    <link rel="stylesheet" href="css/jquery-confirm.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="css/responsive.css">
    <script src="js/fontawesome.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery-confirm.min.js"></script>
    <script src="js/ckeditor/ckeditor.js"></script>

</head>
<body>
<?php if (isset($_SESSION['user_admin'])) { ?>
    <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #e3f2fd;">
        <a class="navbar-brand" href="./"><img src="../src/images/favicon.png" width="70px" alt="Logo"/></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02"
                aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item" title="Головна">
                    <a class="nav-link btn <?php if (!$_GET) {
                        echo "btn-info";
                    } else {
                        echo "btn-outline-info";
                    } ?>" href="./" style="color:<?php if (!$_GET) {
                        echo "#fff!important";
                    } ?>;"><i class="fas fa-home fa-lg"></i> Головна</a>
                </li>
                <li class="nav-item" title="Користувачі">
                    <a class="nav-link btn <?php if (isset($_GET['all_user'])) {
                        echo "btn-info";
                    } else {
                        echo "btn-outline-info";
                    } ?>" href="?all_user" style="color:<?php if (isset($_GET['all_user'])) {
                        echo "#fff!important";
                    } ?>;"><i class="fas fa-users fa-lg"></i> Користувачі</a>
                </li>
                <li class="nav-item" title="Студенти">
                    <a class="nav-link btn <?php if (isset($_GET['students'])) {
                        echo "btn-info";
                    } else {
                        echo "btn-outline-info";
                    } ?>" href="?students" style="color:<?php if (isset($_GET['students'])) {
                        echo "#fff!important";
                    } ?>;"><i class='fas fa-user-graduate fa-lg'></i> Студенти</a>
                </li>
                <li class="nav-item" title="Групи">
                    <a class="nav-link btn <?php if (isset($_GET['groups'])) {
                        echo "btn-info";
                    } else {
                        echo "btn-outline-info";
                    } ?>" href="?groups" style="color:<?php if (isset($_GET['groups'])) {
                        echo "#fff!important";
                    } ?>;"><i class="fa fa-object-group fa-lg"></i> Групи</a>
                </li>
                <li class="nav-item" title="Спеціальності">
                    <a class="nav-link btn <?php if (isset($_GET['specialty'])) {
                        echo "btn-info";
                    } else {
                        echo "btn-outline-info";
                    } ?>" href="?specialty" style="color:<?php if (isset($_GET['specialty'])) {
                        echo "#fff!important";
                    } ?>;"><i class="fa fa-graduation-cap fa-lg"></i> Спеціальності</a>
                </li>
                <li class="nav-item" title="Факультети">
                    <a class="nav-link btn <?php if (isset($_GET['faculties'])) {
                        echo "btn-info";
                    } else {
                        echo "btn-outline-info";
                    } ?>" href="?faculties" style="color:<?php if (isset($_GET['faculties'])) {
                        echo "#fff!important";
                    } ?>;"><i class="fa fa-university fa-lg"></i> Факультети</a>
                </li>
                <li class="nav-item" title="Документи">
                    <a class="nav-link btn <?php if (isset($_GET['documentation'])) {
                        echo "btn-info";
                    } else {
                        echo "btn-outline-info";
                    } ?>" href="?documentation" style="color:<?php if (isset($_GET['documentation'])) {
                        echo "#fff!important";
                    } ?>;"><i class="fa fa-file fa-lg"></i> Документи</a>
                </li>
            </ul>
            <form class="form-inline my-2 my-lg-0 MD">
                <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                    <li class="nav-item" title="На сайт">
                        <a class="nav-link right btn btn-success" href=".././" target="_blank">На сайт <i
                                    class="fas fa-share fa-lg"></i></a>
                    </li>
                    <li class="nav-item" title="Вихід">
                        <a class="nav-link right btn btn-danger" href="?exit_admin"><i
                                    class="fas fa-power-off fa-lg"></i></a>
                    </li>
                </ul>
            </form>
        </div>
    </nav>
    <?php
    if (!$_GET) include 'modules/home.php';
    if (isset($_GET['all_user'])) include 'modules/all_user.php';
    if (isset($_GET['students'])) include 'modules/students.php';
    if (isset($_GET['groups'])) include 'modules/groups.php';
    if (isset($_GET['specialty'])) include 'modules/specialty.php';
    if (isset($_GET['faculties'])) include 'modules/faculties.php';
    if (isset($_GET['documentation'])) include 'modules/documentation.php';

    if (isset($_GET['all_user'])) {
        print "<script> $('#editAvatar, #editPass, #addUser').on('hide.bs.modal', function (e) { document.location.href = '?all_user'; }) </script>";
    }

    if (isset($_GET['students'])) {
        print "<script> $('#addStudent, #editStudent').on('hide.bs.modal', function (e) { document.location.href = '?students'; }) </script>";
    }

    if (isset($_GET['groups'])) {
        print "<script> $('#addGroup').on('hide.bs.modal', function (e) { document.location.href = '?groups'; }) </script>";
    }

    if (isset($_GET['specialty'])) {
        print "<script> $('#addSpecialty').on('hide.bs.modal', function (e) { document.location.href = '?specialty'; }) </script>";
    }

    if (isset($_GET['faculties'])) {
        print "<script> $('#addFaculty').on('hide.bs.modal', function (e) { document.location.href = '?faculties'; }) </script>";
    }

    if (isset($_GET['documentation'])) {
        print "<script> $('#addDoc').on('hide.bs.modal', function (e) { document.location.href = '?documentation'; }) </script>";
    }

} else {
    if (!$_GET)
        include 'modules/autoriz/login.php';
    if (isset($_GET['reg']))
        include 'modules/autoriz/registr.php';
    if (!$_GET) {
        print "<script> $('#Support').on('hide.bs.modal', function (e) { document.location.href = './'; }) </script>";
    }
}
?>

<div id="divModal"></div>
<script>
    $(document).ready(function () {
        $(function () {
            $('#table111').on('refresh.bs.table', function () {
                setTimeout(function () {
                    myInit();
                }, 2000);
            });
            $('#table111').on('toggle.bs.table', function () {
                setTimeout(function () {
                    myInit();
                }, 500);
            });
            $('#table111').on('search.bs.table sort.bs.table', function () {
                setTimeout(function () {
                    myInit();
                }, 1500);
            });
            $('#table111').on('column-search.bs.table', function () {
                setTimeout(function () {
                    myInit();
                }, 1700);
            });
            $('#table111').on('page-change.bs.table', function (size, number) {
                $('#alertInform').html('');
                setTimeout(function () {
                    myInit();
                }, 1900);
            });
            setTimeout(function () {
                myInit();
            }, 3300);
        });
    });
</script>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="js/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bt/bootstrap-table.min.js"></script>
<script src="js/bt/jspdf.min.js"></script>
<script src="js/bt/bootstrap-table-print.min.js"></script>
<script src="js/bt/bootstrap-table-locale-all.min.js"></script>
<script src="js/bt/bootstrap-table-export.min.js"></script>
<script src="js/bt/tableExport.min.js"></script>
<script src="js/bt/bootstrap-table-mobile.js"></script>
<script src="js/bt/bootstrap-table-filter-control.min.js"></script>
<script src="js/bt/bootstrap-editable.min.js"></script>
<script src="js/bt/bootstrap-table-editable.min.js"></script>
</body>
</html>